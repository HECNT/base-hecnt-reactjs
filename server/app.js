var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var ctrl = require('./controllers/login');
var ctrlInicio = require('./controllers/inicio');
const PORT = 3001

app.use(bodyParser.json());
app.use(bodyParser());

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});


app.use(express.static(__dirname + '/public'))

app.post('/do-magia', doMagia);

function doMagia(req, res) {
  var d = req.body;
  ctrl.doMagia(d)
  .then(function(result){
    res.json(result);
  })
}


app.listen(PORT, function(){
  console.log(`Escuchando en el puerto ${PORT}`);
})
