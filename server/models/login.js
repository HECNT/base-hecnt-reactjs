const { Pool, Client } = require('pg')
var fs = require('fs');

module.exports.doMagia = function(d) {
  return new Promise(function(resolve, reject) {
    var config = fs.readFileSync(__dirname + '/sql_cred','utf8')
    config = JSON.parse(config);
    config.user = d.usuario;
    config.password = d.password;
    const pool = new Pool(config)

    pool.query('SELECT 1+1 AS res', (err, res) => {
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve({err: false, data: res.rows})
      }
      pool.end()
    })

  });
}
