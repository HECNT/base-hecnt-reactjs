const { Pool, Client } = require('pg')
var fs = require('fs');

module.exports.getInit = function(d) {
  return new Promise(function(resolve, reject) {
    var config = fs.readFileSync(__dirname + '/sql_cred','utf8')
    config = JSON.parse(config);
    config.user = d.usuario;
    config.password = d.password;
    const pool = new Pool(config)

    var query = `

    select
    	*,
    	(
    		SELECT
    			count(a1.programador_id) as total_lenguajes
    		FROM
    			programador_lenguaje AS a1
    		INNER JOIN
    			programador AS a2
    		ON
    			a1.programador_id = a2.programador_id
    		INNER JOIN
    			lenguaje AS a3
    		ON
    			a1.lenguaje_id = a3.lenguaje_id
    		where
    			a1.activo = true
    		and
    			a1.programador_id = a4.programador_id
    		group by
    			a1.programador_id
    	)
    from
    	programador as a4
    where
  	  a4.activo = true

    `

    pool.query(query, (err, res) => {
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve({err: false, data: res.rows})
      }
      pool.end()
    })

  });
}
