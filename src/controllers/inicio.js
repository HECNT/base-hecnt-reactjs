import React, { Component } from 'react';
import axios from 'axios';
const uri = "http://localhost:3001/";
class inicio extends React.Component {

  static getInit(d){
    return new Promise(function(resolve, reject) {
      axios.post(uri + 'get-init', d)
      .then(function(res) {
        resolve(res);
      }).catch(error => {
        resolve({err: true, description: error})
      })
    });;
  }

}

export default inicio;
