import React, { Component } from 'react';
import { Button, Table, Alert, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Input, Label } from 'reactstrap';
import inicio from '../controllers/inicio.js'

class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      usuario: true,
      password: true,
      session: false
    };
  }

  async componentWillMount() {
    var session = localStorage.getItem('session');
    if (session) {
      session = JSON.parse(session);
      this.setState({session: session});
    } else {
      this.setState({session: false})
    }
  }

  logOut() {
    localStorage.setItem("session", null)
    window.location.reload()
  }


   render() {
     if (!this.state.session) {
       return (
         <div>
           <Alert color="warning">
             No tienes una session
           </Alert>
         </div>
       )
     } else {
       return (
          <div>
             <h4>Inicio</h4>
             <Button color="danger" onClick={() => this.logOut()}>Cerrar sesión</Button>
          </div>
       );
     }
   }
}
export default Home;
